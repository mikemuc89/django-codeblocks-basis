from django.urls import path

from basis.routes import app_routes


urlpatterns = [
    path(url, resource.as_view(), name=resource.name) for (url, resource) in app_routes
]
