from django.core.management.base import BaseCommand
from django.core.management.commands import test
from django.conf import settings


class Command(test.Command):
    args = ''
    help = 'Test all of MY_INSTALLED_APPS + framework'

    def handle(self, *args, **options):
        super(Command, self).handle(*(settings.MY_INSTALLED_APPS + ['framework'] + list(args)), **options)
