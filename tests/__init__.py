from django.apps import apps
from django.test import TestCase

from basis.apps import BasisConfig
from basis.forms.tests import *
from basis.models.tests import *
from basis.utils.tests import *
from basis.views.tests import *


class ConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(BasisConfig.name, 'basis')
        self.assertEqual(apps.get_app_config('basis').name, 'basis')
