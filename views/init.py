from framework.fields import SearchField
from framework.views import ResourceView

from authorization.models import UserData
from basis.models import (
    HamburgerItem,
    Link,
    MenuItem,
    Notification,
)


class InitView(ResourceView):
    name = 'basis.init'

    def render(self):
        return {
            'data': {
                'links': [x.render() for x in Link.get_visible()],
                'menu': [x.render() for x in MenuItem.get_visible()],
                'notifications': [x.render() for x in filter(lambda x: x.identifier not in self.request.COOKIES, Notification.get_visible())],
                'user': None,
            },
            'fields': {
                'search': SearchField().render(),
            },
        }
