from basis.forms import NotificationAddForm
from basis.models import Notification
from framework.views import CreateFlowView
from authorization.views.mixins import WithPreauth


class NotificationAddView(CreateFlowView, WithPreauth):
    Form = NotificationAddForm
    Model = Notification

    name = 'basis.notification.add'
    need_authentication = True
    need_superuser_rights = True
