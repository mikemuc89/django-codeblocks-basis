from datetime import datetime
from framework.views import ModelResourceView
from basis.models import Notification


class NotificationGetView(ModelResourceView):
    Model = Notification
    name = 'basis.notification.get'
