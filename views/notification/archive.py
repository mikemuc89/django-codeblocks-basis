from basis.models import Notification
from framework.views import ArchiveFlowView
from authorization.views.mixins import WithPreauth


class NotificationArchiveView(ArchiveFlowView, WithPreauth):
    Model = Notification

    name = 'basis.notification.archive'
    need_authentication = True
    need_superuser_rights = True
    SESSKEY_ID = 'basis.notification.archive.id'
