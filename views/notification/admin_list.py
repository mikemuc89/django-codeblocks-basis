from framework.views import AdminTableView
from basis.forms import NotificationAdminFilterForm
from basis.models import Notification
from authorization.views.mixins import WithPreauth


class NotificationAdminListView(AdminTableView, WithPreauth):
    name = 'basis.notification.admin_list'
    Model = Notification
    FilterForm = NotificationAdminFilterForm

    filters = (
        AdminTableView.Filters.Search('search', fields=('href', 'identifier', 'text')),
        AdminTableView.Filters.Basic('visible'),
    )
    sorting = (
        AdminTableView.Sorting('text', default=AdminTableView.Sorting.DSC),
        AdminTableView.Sorting('href'),
    )
