from basis.forms import NotificationAddForm
from basis.models import Notification
from framework.views import ModifyFlowView
from authorization.views.mixins import WithPreauth


class NotificationModifyView(ModifyFlowView, WithPreauth):
    Form = NotificationAddForm
    Model = Notification

    name = 'basis.notification.modify'
    need_authentication = True
    need_superuser_rights = True
    SESSKEY_ID = 'basis.notification.modify.id'
