import mock
from datetime import (
    datetime,
    timedelta,
)

from framework.test import BaseTestCase

from authorization.models import (
    User,
    UserData,
)
from basis.models import (
    Link,
    MenuItem,
    Notification,
)


def mock_function(*args):
    return True

def raise_exception(*args):
    raise Exception()


class InitResourceViewTestCase(BaseTestCase):
    flow_name = 'basis.init'
    url = '/api/init'

    def prepare_test(self):
        data = self.load_data(__file__, './mocks.json')

        for user in data['users']:
            u = User.objects.create_user(
                user['username'],
                user['email'],
                user['password']
            )
            u.last_name = user['last_name']
            u.first_name = user['first_name']
            u.is_active = user['is_active']
            u.is_superuser = user['is_superuser']
            u.save()
            dt = user['user_data']
            token_delta = dt.pop('token_expires_delta_days', 1)
            d = UserData.objects.create(
                token_expires=datetime.now() + timedelta(days=token_delta),
                user=u,
                **dt
            )
            d.save()

        for kwargs in data['notifications']:
            start_delta = kwargs.pop('publication_start_delta_days', None)
            end_delta = kwargs.pop('publication_end_delta_days', None)
            Notification.objects.create(
                publication_start=datetime.now() + timedelta(days=start_delta) if start_delta else None,
                publication_end=datetime.now() + timedelta(days=end_delta) if end_delta else None,
                **kwargs
            )

        for kwargs in data['links']:
            Link.objects.create(**kwargs)

        for kwargs in data['menus']:
            MenuItem.objects.create(**kwargs)

    def test_positive_guest(self, *args):
        request = {}
        expected = {
            '~data': {
                'links': [{
                    'href': 'http://www.onet.pl',
                    'id': 'basic_all',
                    'text': 'Basic for all'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'order_2',
                    'text': 'Order 2'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'icon',
                    'text': 'Has icon order 3',
                    'icon': 'exclamation'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'basic_guest',
                    'text': 'Basic for guest only'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'only_icon',
                    'icon': 'exclamation',
                }],
                'menu': [{
                    'href': '#/AUTH/LOGIN',
                    'id': 'order_2',
                    'text': 'Order 2'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'icon',
                    'text': 'Has icon order 3',
                    'icon': 'exclamation'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'basic_guest',
                    'text': 'Basic for guest only'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'basic_all',
                    'text': 'Basic for all'
                }],
                'notifications': [{
                        'id': 'basic_all',
                        'text': 'Basic for all',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'basic_guest',
                        'text': 'Basic for guest only',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'icon',
                        'text': 'Has icon',
                        'icon': 'exclamation',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'action',
                        'text': 'Has action without meta',
                        'action': {
                            'text': 'Action Text',
                            'meta': {}
                        },
                        'kind': 'GENERAL'
                    }, {
                        'id': 'action_meta',
                        'text': 'Has action with meta',
                        'action': {
                            'text': 'Action Text',
                            'meta': {
                                'data': {
                                    'param1': 'value1',
                                    'param2': 'value2'
                                }
                            }
                        },
                        'kind': 'GENERAL'
                    }, {
                        'id': 'non_default_kind',
                        'text': 'Non default kind',
                        'kind': 'NOTIFICATION.KINDS.ERROR'
                    }, {
                        'id': 'inside_dates',
                        'text': 'both dates, inside',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'valid_link',
                        'text': 'Valid link',
                        'kind': 'GENERAL',
                        'link': {
                            'href': '#/ASD/ABC',
                            'text': 'Link text'
                        }
                    }, {
                        'id': 'link_no_href',
                        'text': 'Link, no href',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'link_no_text',
                        'text': 'Link, no text',
                        'kind': 'GENERAL'
                    }
                ],
                'user': None
            },
            '~fields': {
                '~search': {
                    'errors': None,
                    '~widget': {
                        'field_type': 'str'
                    },
                    'value': ''
                }
            }
        }

        self.assertResponse(request, expected)

    def test_positive_user(self, *args):
        self.client.force_login(User.objects.get_or_create(username='activeuser')[0])

        request = {}
        expected = {
            '~data': {
                'links': [{
                    'href': 'http://www.onet.pl',
                    'id': 'basic_all',
                    'text': 'Basic for all'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'basic_user',
                    'text': 'Basic for user only order 1'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'order_2',
                    'text': 'Order 2'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'icon',
                    'text': 'Has icon order 3',
                    'icon': 'exclamation'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'only_icon',
                    'icon': 'exclamation',
                }],
                'menu': [{
                    'href': '#/AUTH/LOGIN',
                    'id': 'basic_user',
                    'text': 'Basic for user only order 1'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'order_2',
                    'text': 'Order 2'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'icon',
                    'text': 'Has icon order 3',
                    'icon': 'exclamation'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'basic_all',
                    'text': 'Basic for all'
                }],
                'notifications': [{
                        'id': 'basic_all',
                        'text': 'Basic for all',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'basic_user',
                        'text': 'Basic for user only',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'icon',
                        'text': 'Has icon',
                        'icon': 'exclamation',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'action',
                        'text': 'Has action without meta',
                        'action': {
                            'text': 'Action Text',
                            'meta': {}
                        },
                        'kind': 'GENERAL'
                    }, {
                        'id': 'action_meta',
                        'text': 'Has action with meta',
                        'action': {
                            'text': 'Action Text',
                            'meta': {
                                'data': {
                                    'param1': 'value1',
                                    'param2': 'value2'
                                }
                            }
                        },
                        'kind': 'GENERAL'
                    }, {
                        'id': 'non_default_kind',
                        'text': 'Non default kind',
                        'kind': 'NOTIFICATION.KINDS.ERROR'
                    }, {
                        'id': 'inside_dates',
                        'text': 'both dates, inside',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'valid_link',
                        'text': 'Valid link',
                        'kind': 'GENERAL',
                        'link': {
                            'href': '#/ASD/ABC',
                            'text': 'Link text'
                        }
                    }, {
                        'id': 'link_no_href',
                        'text': 'Link, no href',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'link_no_text',
                        'text': 'Link, no text',
                        'kind': 'GENERAL'
                    }
                ],
                '~user': {
                    'email': 'active@email.com',
                    'username': 'activeuser'
                }
            },
            '~fields': {
                '~search': {
                    'errors': None,
                    '~widget': {
                        'field_type': 'str'
                    },
                    'value': ''
                }
            }
        }

        self.assertResponse(request, expected)

    def test_positive_admin(self, *args):
        self.client.force_login(User.objects.get_or_create(username='adminuser')[0])

        request = {}
        expected = {
            'data': {
                'links': [{
                    'href': 'http://www.onet.pl',
                    'id': 'basic_all',
                    'text': 'Basic for all'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'order_2',
                    'text': 'Order 2'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'icon',
                    'text': 'Has icon order 3',
                    'icon': 'exclamation'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'basic_admin',
                    'text': 'Basic for admin only'
                }, {
                    'href': 'http://www.onet.pl',
                    'id': 'only_icon',
                    'icon': 'exclamation',
                }],
                'menu': [{
                    'href': '#/AUTH/LOGIN',
                    'id': 'order_2',
                    'text': 'Order 2'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'icon',
                    'text': 'Has icon order 3',
                    'icon': 'exclamation'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'basic_admin',
                    'text': 'Basic for admin only'
                }, {
                    'href': '#/AUTH/LOGIN',
                    'id': 'basic_all',
                    'text': 'Basic for all'
                }],
                'notifications': [{
                        'id': 'basic_all',
                        'text': 'Basic for all',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'basic_admin',
                        'text': 'Basic for admin only',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'icon',
                        'text': 'Has icon',
                        'icon': 'exclamation',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'action',
                        'text': 'Has action without meta',
                        'action': {
                            'text': 'Action Text',
                            'meta': {}
                        },
                        'kind': 'GENERAL'
                    }, {
                        'id': 'action_meta',
                        'text': 'Has action with meta',
                        'action': {
                            'text': 'Action Text',
                            'meta': {
                                'data': {
                                    'param1': 'value1',
                                    'param2': 'value2'
                                }
                            }
                        },
                        'kind': 'GENERAL'
                    }, {
                        'id': 'non_default_kind',
                        'text': 'Non default kind',
                        'kind': 'NOTIFICATION.KINDS.ERROR'
                    }, {
                        'id': 'inside_dates',
                        'text': 'both dates, inside',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'valid_link',
                        'text': 'Valid link',
                        'kind': 'GENERAL',
                        'link': {
                            'href': '#/ASD/ABC',
                            'text': 'Link text'
                        }
                    }, {
                        'id': 'link_no_href',
                        'text': 'Link, no href',
                        'kind': 'GENERAL'
                    }, {
                        'id': 'link_no_text',
                        'text': 'Link, no text',
                        'kind': 'GENERAL'
                    }
                ],
                'user': {
                    'email': 'admin@email.com',
                    'username': 'adminuser'
                }
            },
            'fields': {
                'search': {
                    'errors': None,
                    'widget': {
                        'field_type': 'str'
                    },
                    'value': ''
                }
            }
        }

        self.assertResponse(request, expected)

    @mock.patch('basis.views.init.InitView.guest_render', side_effect=raise_exception)
    def test_generic_error(self, *args):
        request = {}
        expected = {
            'step': 'error',
            'errors': 'Błąd podczas przetwarzania żądania.',
        }

        self.assertResponse(request, expected)
