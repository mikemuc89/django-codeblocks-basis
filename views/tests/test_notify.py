import mock
from framework.test import BaseTestCase


def raise_exception(*args):
    raise Exception()


class NotifyResourceViewTestCase(BaseTestCase):
    flow_name = 'basis.notify'
    url = '/api/notify'

    def test_positive(self, *args):
        request = {
            'data': {
                'id': 'test',
                'action': 'submit'
            }
        }
        expected = {}

        self.assertResponse(request, expected)

        self.assertTrue(self.client.cookies.get('test', None) is not None)
        self.assertTrue(self.client.cookies.get('test', None).value == 'submit')
        self.assertTrue(self.client.cookies.get('action', None) is None)

        request = {
            'data': {
                'id': 'test2',
                'action': 'submit2'
            }
        }
        expected = {}

        self.assertResponse(request, expected)

        self.assertTrue(self.client.cookies.get('test', None) is not None)
        self.assertTrue(self.client.cookies.get('test', None).value == 'submit')
        self.assertTrue(self.client.cookies.get('test2', None) is not None)
        self.assertTrue(self.client.cookies.get('test2', None).value == 'submit2')
        
    def test_wrong_parameters(self, *args):
        request = {
            'data': {
                'id': 'action_does_not_exist'
            }
        }
        expected = {
            'step': 'error',
            'errors': 'Błędne dane.',
        }

        self.assertResponse(request, expected)

    @mock.patch('basis.views.notify.NotifyView.render', side_effect=raise_exception)
    def test_generic_error(self, *args):
        request = {
            'data': {
                'id': 'test',
                'action': 'submit'
            }
        }
        expected = {
            'step': 'error',
            'errors': 'Błąd podczas przetwarzania żądania.',
        }

        self.assertResponse(request, expected)
