from framework.errors import (
    ReqIvldRequestException,
)
from framework.views import ResourceView


class NotifyView(ResourceView):
    name = 'basis.notify'

    def render(self):
        data = self.body.get('data', {})
        action = data.get('action', None)
        id = data.get('id', None)

        if not action or not id:
            raise ReqIvldRequestException()

        return {
            '__set_cookie': {
                'key': id,
                'value': action,
            },
        }
