from datetime import datetime
from framework.views import ModelResourceView
from basis.models import Link


class LinkGetView(ModelResourceView):
    Model = Link
    name = 'basis.link.get'
