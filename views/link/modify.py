from basis.forms import LinkAddForm
from basis.models import Link
from framework.views import ModifyFlowView
from authorization.views.mixins import WithPreauth


class LinkModifyView(ModifyFlowView, WithPreauth):
    Form = LinkAddForm
    Model = Link

    name = 'basis.link.modify'
    need_authentication = True
    need_superuser_rights = True
    SESSKEY_ID = 'basis.link.modify.id'
