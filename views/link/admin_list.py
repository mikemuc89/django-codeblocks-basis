from framework.views import AdminTableView
from basis.forms import LinkAdminFilterForm
from basis.models import Link
from authorization.views.mixins import WithPreauth


class LinkAdminListView(AdminTableView, WithPreauth):
    name = 'basis.link.admin_list'
    Model = Link
    FilterForm = LinkAdminFilterForm

    filters = (
        AdminTableView.Filters.Search('search', fields=('href', 'identifier', 'text')),
        AdminTableView.Filters.Basic('visible'),
    )
    sorting = (
        AdminTableView.Sorting('text', default=AdminTableView.Sorting.DSC),
        AdminTableView.Sorting('href')
    )
