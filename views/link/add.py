from basis.forms import LinkAddForm
from basis.models import Link
from framework.views import CreateFlowView
from authorization.views.mixins import WithPreauth


class LinkAddView(CreateFlowView, WithPreauth):
    Form = LinkAddForm
    Model = Link

    name = 'basis.link.add'
    need_authentication = True
    need_superuser_rights = True
