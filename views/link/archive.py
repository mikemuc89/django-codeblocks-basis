from basis.models import Link
from framework.views import ArchiveFlowView
from authorization.views.mixins import WithPreauth


class LinkArchiveView(ArchiveFlowView, WithPreauth):
    Model = Link

    name = 'basis.link.archive'
    need_authentication = True
    need_superuser_rights = True
    SESSKEY_ID = 'basis.link.archive.id'
