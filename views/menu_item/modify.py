from basis.forms import MenuItemAddForm
from basis.models import MenuItem
from framework.views import ModifyFlowView
from authorization.views.mixins import WithPreauth


class MenuItemModifyView(ModifyFlowView, WithPreauth):
    Form = MenuItemAddForm
    Model = MenuItem

    name = 'basis.menu_item.modify'
    need_authentication = True
    need_superuser_rights = True
    SESSKEY_ID = 'basis.menu_item.modify.id'
