from basis.forms import MenuItemAddForm
from basis.models import MenuItem
from framework.views import CreateFlowView
from authorization.views.mixins import WithPreauth


class MenuItemAddView(CreateFlowView, WithPreauth):
    Form = MenuItemAddForm
    Model = MenuItem

    name = 'basis.menu_item.add'
    need_authentication = True
    need_superuser_rights = True
