from datetime import datetime
from framework.views import ModelResourceView
from basis.models import MenuItem


class MenuItemGetView(ModelResourceView):
    Model = MenuItem
    name = 'basis.menu_item.get'
