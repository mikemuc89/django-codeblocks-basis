from framework.views import AdminTableView
from basis.forms import MenuItemAdminFilterForm
from basis.models import MenuItem
from authorization.views.mixins import WithPreauth


class MenuItemAdminListView(AdminTableView, WithPreauth):
    name = 'basis.menu_item.admin_list'
    Model = MenuItem
    FilterForm = MenuItemAdminFilterForm

    filters = (
        AdminTableView.Filters.Search('search', fields=('href', 'identifer', 'text')),
        AdminTableView.Filters.Basic('visible'),
    )
    sorting = (
        AdminTableView.Sorting('text', default=AdminTableView.Sorting.DSC),
        AdminTableView.Sorting('href')
    )
