from basis.models import MenuItem
from framework.views import ArchiveFlowView
from authorization.views.mixins import WithPreauth


class MenuItemArchiveView(ArchiveFlowView, WithPreauth):
    Model = MenuItem

    name = 'basis.menu_item.archive'
    need_authentication = True
    need_superuser_rights = True
    SESSKEY_ID = 'basis.menu_item.archive.id'
