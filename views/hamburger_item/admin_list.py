from framework.views import AdminTableView
from basis.forms import HamburgerItemAdminFilterForm
from basis.models import HamburgerItem
from authorization.views.mixins import WithPreauth


class HamburgerItemAdminListView(AdminTableView, WithPreauth):
    name = 'basis.hamburger_item.admin_list'
    Model = HamburgerItem
    FilterForm = HamburgerItemAdminFilterForm

    filters = (
        AdminTableView.Filters.Search('search', fields=('href', 'identifier', 'text')),
        AdminTableView.Filters.Basic('visible'),
    )
    sorting = (
        AdminTableView.Sorting('text', default=AdminTableView.Sorting.DSC),
        AdminTableView.Sorting('href')
    )
