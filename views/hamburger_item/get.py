from datetime import datetime
from framework.views import ModelResourceView
from basis.models import HamburgerItem


class HamburgerItemGetView(ModelResourceView):
    Model = HamburgerItem
    name = 'basis.hamburger_item.get'
