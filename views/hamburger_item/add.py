from basis.forms import HamburgerItemAddForm
from basis.models import HamburgerItem
from framework.views import CreateFlowView
from authorization.views.mixins import WithPreauth


class HamburgerItemAddView(CreateFlowView, WithPreauth):
    Form = HamburgerItemAddForm
    Model = HamburgerItem

    name = 'basis.hamburger_item.add'
    need_authentication = True
    need_superuser_rights = True
