from basis.forms import HamburgerItemAddForm
from basis.models import HamburgerItem
from framework.views import ModifyFlowView
from authorization.views.mixins import WithPreauth


class HamburgerItemModifyView(ModifyFlowView, WithPreauth):
    Form = HamburgerItemAddForm
    Model = HamburgerItem

    name = 'basis.hamburger_item.modify'
    need_authentication = True
    need_superuser_rights = True
    SESSKEY_ID = 'basis.hamburger_item.modify.id'
