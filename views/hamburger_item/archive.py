from basis.models import HamburgerItem
from framework.views import ArchiveFlowView
from authorization.views.mixins import WithPreauth


class HamburgerItemArchiveView(ArchiveFlowView, WithPreauth):
    Model = HamburgerItem

    name = 'basis.hamburger_item.archive'
    need_authentication = True
    need_superuser_rights = True
    SESSKEY_ID = 'basis.hamburger_item.archive.id'
