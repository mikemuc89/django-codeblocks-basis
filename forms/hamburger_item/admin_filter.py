from framework.forms import BaseForm
from framework.fields import (
    NullableBoolField,
    SearchField,
)


class HamburgerItemAdminFilterForm(BaseForm):
    search = SearchField()
    visible = NullableBoolField()
