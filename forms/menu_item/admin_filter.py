from framework.forms import BaseForm
from framework.fields import (
    NullableBoolField,
    SearchField,
)


class MenuItemAdminFilterForm(BaseForm):
    search = SearchField()
    visible = NullableBoolField()
