from framework.forms import BaseForm
from framework.fields import (
    BoolField,
    ChoiceField,
    HrefField,
    OrderField,
    TextField,
)
from basis.models import MenuItem


class MenuItemAddForm(BaseForm):
    href = HrefField(required=True)
    icon = TextField()
    identifier = TextField()
    order = OrderField()
    parent = ChoiceField(items=MenuItem.render_choice)
    text = TextField()
    visible = BoolField(default_value=True)
