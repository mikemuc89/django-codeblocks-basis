from framework.forms import BaseForm
from framework.fields import (
    NullableBoolField,
    SearchField,
)


class LinkAdminFilterForm(BaseForm):
    search = SearchField()
    visible = NullableBoolField()
