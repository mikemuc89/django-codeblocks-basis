from framework.forms import BaseForm
from framework.fields import (
    BoolField,
    HrefField,
    OrderField,
    TextField,
)


class LinkAddForm(BaseForm):
    href = HrefField(required=True)
    icon = TextField()
    identifier = TextField()
    order = OrderField()
    text = TextField()
    visible = BoolField(default_value=True)
