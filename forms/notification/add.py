from framework.forms import BaseForm
from framework.fields import (
    ActionFieldset,
    BoolField,
    ChoiceField,
    DateTimeFieldset,
    LinkFieldset,
    MarkdownField,
    TextField,
)


class NotificationAddForm(BaseForm):
    action = ActionFieldset()
    content = MarkdownField(required=True)
    icon = TextField()
    identifier = TextField(required=True)
    kind = ChoiceField()
    link = LinkFieldset()
    publication_date = DateTimeFieldset()
    visible = BoolField(default_value=True)
