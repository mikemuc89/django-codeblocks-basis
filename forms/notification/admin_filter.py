from framework.forms import BaseForm
from framework.fields import (
    BoolField,
    DateRangeFieldset,
    NullableBoolField,
    SearchField,
)


class NotificationAdminFilterForm(BaseForm):
    date = DateRangeFieldset()
    has_action = BoolField()
    has_link = BoolField()
    search = SearchField()
    visible = NullableBoolField()
