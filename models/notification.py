import json
from datetime import datetime
from django.db import models
import framework.fields as form_fields
from framework.model_fields import KindField
from framework.models import EditableModel
from framework.models.mixins import WithAction, WithLink, WithPublicationDate, WithVisibility
from framework.models.utils import q
from authorization.models.mixins import WithAccessLevel


QUERY_SEARCH = q.query_search('content', 'identifier')


class Form(WithAction.Form, WithLink.Form, WithPublicationDate.Form, WithVisibility.Form, WithAccessLevel.Form):
    content = form_fields.MarkdownField(required=True)
    icon = form_fields.TextField()
    identifier = form_fields.IdField()
    kind = form_fields.TextField()


class Filter(WithAction.Filter, WithLink.Filter, WithPublicationDate.Filter, WithVisibility.Filter, WithAccessLevel.Filter):
    search = form_fields.SearchField(query=QUERY_SEARCH, merge_strategy='any')


class AdminFilter(WithAction.AdminFilter, WithLink.AdminFilter, WithPublicationDate.AdminFilter, WithVisibility.AdminFilter, WithAccessLevel.AdminFilter):
    search = form_fields.SearchField(query=QUERY_SEARCH, merge_strategy='any')


class Notification(EditableModel, WithAction, WithLink, WithPublicationDate, WithVisibility, WithAccessLevel):
    content = models.TextField()
    icon = models.CharField(blank=True, null=True, max_length=64)
    identifier = models.CharField(max_length=64)
    kind = KindField(default=KindField.CHOICES.PRIMARY)

    Form = Form
    Filter = Filter
    AdminFilter = AdminFilter

    def _render_admin_table_item(self):
        return { 'content': self.content, 'id': self.identifier, 'icon': self.icon, 'kind': self.kind }
    
    def _render_choice_item_data(self):
        return { 'id': self.identifier, **({ 'icon': self.icon } if self.icon else {}), **({ 'kind': self.kind } if self.kind else {}) }
    
    def _render_table_item(self):
        return { 'content': self.content, 'id': self.identifier, **({ 'icon': self.icon } if self.icon else {}), **({ 'kind': self.kind } if self.kind else {}) }

    def _render(self):
        return { 'content': self.content, 'id': self.identifier, **({ 'icon': self.icon } if self.icon else {}), **({ 'kind': self.kind } if self.kind else {}) }

    def _render_form(self):
        return { 'content': self.content, 'id': self.identifier, 'icon': self.icon, 'kind': self.kind }

    def _render_short(self):
        return { 'id': self.identifier, **({ 'icon': self.icon } if self.icon else {}), **({ 'kind': self.kind } if self.kind else {}) }

    class Meta:
        app_label = 'basis'
