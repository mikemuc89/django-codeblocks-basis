from framework.models import LinkModel
from authorization.models.mixins import WithAccessLevel


class Form(LinkModel.Form, WithAccessLevel.Form):
    pass


class Filter(LinkModel.Filter, WithAccessLevel.Filter):
    pass


class AdminFilter(LinkModel.AdminFilter, WithAccessLevel.AdminFilter):
    pass


class Link(LinkModel, WithAccessLevel):
    Form = Form
    Filter = Filter
    AdminFilter = AdminFilter

    class Meta:
        app_label = 'basis'
