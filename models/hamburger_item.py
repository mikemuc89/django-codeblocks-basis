from django.db import models
import framework.fields as form_fields
from framework.models import LinkModel
from framework.models.mixins import WithParent
from framework.models.utils import q
from authorization.models.mixins import WithAccessLevel


QUERY_SEARCH = q.query_search('text')


class Form(LinkModel.Form, WithParent.Form, WithAccessLevel.Form):
    text = form_fields.TextField(required=True)


class Filter(LinkModel.Filter, WithParent.Filter, WithAccessLevel.Filter):
    search = form_fields.SearchField(query=QUERY_SEARCH, merge_strategy='any')        


class AdminFilter(LinkModel.AdminFilter, WithParent.AdminFilter, WithAccessLevel.AdminFilter):
    search = form_fields.SearchField(query=QUERY_SEARCH, merge_strategy='any')


class HamburgerItem(LinkModel, WithParent, WithAccessLevel):
    text = models.TextField()

    Form = Form
    Filter = Filter
    AdminFilter = AdminFilter

    class Meta:
        app_label = 'basis'
