from basis import views


app_routes = (
    ('api/init', views.InitView),
    ('api/notify', views.NotifyView),
    # ('api/notification/add', views.AddNotificationView),
    # ('api/notification/archive', views.ArchiveNotificationView),
    # ('api/notification/modify', views.ModifyNotificationView),
    # ('api/search', views.SearchView),
)
